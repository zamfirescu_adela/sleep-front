export function getGroupSchedule(state) {
  return state.groupSchedule;
}

export function getPersonalSchedule(state) {
  return state.personal;
}

export function getClassRooms(state) {
  return state.classrooms;
}

export function getSubjects(state) {
  return state.subjects;
}

export function getTimeUnits(state) {
  return state.timeUnits;
}

export function getDays(state) {
  return state.days;
}

export function getProfessors(state) {
  return state.professors;
}
export function getProfessorsSubjects(state) {
  return state.professorSubjects;
}
export function getRomanianWeekDays(state) {
  return state.romanianWeekDays;
}

// export function getSubjectsByProfessor(state, professorId) {
//   return state.professorSubjects
//     .filter(item => item.professorId === professorId)
//     .map(item =>
//       state.subjects.find(
//         subItem =>
//           subItem.id === item.subjectId || subItem.name === item.subjectName
//       )
//     );
// }
