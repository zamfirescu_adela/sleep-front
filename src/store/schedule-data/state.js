export default {
  romanianWeekDays: [
    { id: 1, name: "Luni" },
    { id: 2, name: "Marti" },
    { id: 3, name: "Miercuri" },
    { id: 4, name: "Joi" },
    { id: 5, name: "Vineri" }
  ],
  groupSchedule: [],
  personal: null,
  professors: [],
  subjects: [],
  timeUnits: [],
  timeUnitsDays: [],
  classrooms: [],
  professorSubjects: []
};
