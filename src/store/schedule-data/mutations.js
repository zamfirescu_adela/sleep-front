export function SET_GROUP_SCHEDULE(state, payload) {
  state.groupSchedule = payload;
}

export function SET_PERSONAL_SCHEDULE(state, payload) {
  state.personalSchedule = payload;
}

export function SET_PROFESSORS(state, payload) {
  state.professors = payload;
}

export function SET_SUBJECTS(state, payload) {
  state.subjects = payload;
}
export function SET_PROFESSOR_SUBJECTS(state, payload) {
  state.professorSubjects = payload;
}

export function SET_TIME_UNITS(state, payload) {
  state.timeUnits = payload;
}
export function SET_TIME_UNITS_DAYS(state, payload) {
  state.timeUnitsDays = payload;
}

export function SET_DAYS(state, payload) {
  state.romanianWeekDays = payload;
}

export function SET_CLASSROOMS(state, payload) {
  state.classrooms = payload;
}
