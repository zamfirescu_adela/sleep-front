import Axios from "axios";

export async function loadGroupSchedule({ commit }, payload) {
  const response = await Axios.get(`/api/schedule?groupId=${payload}`);

  commit("SET_GROUP_SCHEDULE", response.data);
}

export function setPersonalSchedule({ commit }, payload) {
  commit("SET_PERSONAL_SCHEDULE", payload);
}

export async function loadProfessorsSubjects({ commit }) {
  const response = await Axios.get("/api/details/professors");
  const responseSubjects = await Axios.get("/api/details/subjects");

  const professors = response.data.map(item => ({
    ...item,
    subjects: undefined
  }));

  const subjects = responseSubjects.data.map(item => ({
    ...item
  }));

  const professorsSubjects = [];

  response.data.forEach(professor => {
    professor.subjects.forEach(subject => {
      professorsSubjects.push({
        subjectId: subject.id,
        professorId: professor.id,
        subjectName: subject.name
      });
    });
  });

  commit("SET_PROFESSORS", professors);

  commit("SET_SUBJECTS", subjects);

  commit("SET_PROFESSOR_SUBJECTS", professorsSubjects);
}

export async function loadTimeUnits({ commit }) {
  const response = await Axios.get("/api/details/timeunits");
  const timeUnits = response.data.map(item => ({
    ...item
  }));
  const days = [];
  const timeUnitsDays = [];

  response.data.forEach(timeUnit => {
    if (!days.find(item => item.id === timeUnit.day.id)) {
      days.push({ id: timeUnit.day.id, name: timeUnit.day.dayName });
    }
    timeUnitsDays.push({
      timeUnitId: timeUnit.id,
      dayId: timeUnit.day.id
    });
  });

  commit("SET_DAYS", days);

  commit("SET_TIME_UNITS_DAYS", timeUnitsDays);

  commit("SET_TIME_UNITS", timeUnits);
}

export function setDays({ commit }, payload) {
  commit("SET_DAYS", payload);
}

export async function loadClassrooms({ commit }) {
  const response = await Axios.get("/api/details/classrooms");
  commit("SET_CLASSROOMS", response.data);
}
