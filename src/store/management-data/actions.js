import Axios from "axios";

export function setNotifications({ commit }, payload) {
  commit("SET_NOTIFICATIONS", payload);
}

export function setSelectedNotification({ commit }, payload) {
  commit("SET_SELECTED_NOTIFICATION", payload);
}
export async function loadAllSpecializations({ commit }, payload) {
  commit("SET_ALL_SPECIALIZATIONS", payload);
}

export async function loadChangeRequests({ commit }) {
  const response = await Axios.get("/api/changelog");
  commit("SET_CHANGE_REQUESTS", response.data);
}

export async function loadAllGroups({ commit }) {
  const response = await Axios.get("/api/details/groups");
  commit("SET_ALL_GROUPS", response.data);
}

export async function loadAllSeries({ commit }) {
  const response = await Axios.get("/api/details/series");
  commit("SET_ALL_SERIES", response.data);
}

export async function loadAllBuildings({ commit }) {
  const response = await Axios.get("/api/details/buildings");
  commit("SET_ALL_BUILDINGS", response.data);
}
