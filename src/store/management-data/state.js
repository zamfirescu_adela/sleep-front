export default {
  selectedNotification: {
    id: 0,
    title: "",

    description: ""
  },
  operationType: {
    ADD: "Adauga",
    DELETE: "Sterge",
    EDIT: "Modifica"
  },
  changeLogType: {
    professor: "profesor",
    subject: "materie",
    classroom: "sala",
    class: "curs/seminar"
  },
  changeLogStatus: {
    SENT: "TRIMIS",
    RECEIVED: "PRIMIT",
    ACCEPTED: "ACCEPTAT",
    REJECTED: "RESPINS"
  },
  allGroups: [],
  allSeries: [],
  allSpecializations: [
    {
      id: 1,
      name: "Cibernetica"
    },
    {
      id: 2,
      name: "Informatica Economica"
    },
    {
      id: 3,
      name: "Statistica"
    }
  ],
  allBuildings: [],

  changeRequests: [],

  notifications: [
    {
      id: 1,
      title: "Notification 1",
      from: "Mary Jane",
      description: "Here is your description"
    },
    {
      id: 2,
      title: "Notification 2",
      from: "Mary Jane",
      description: "Here is your description"
    },
    {
      id: 3,
      title: "Notification 3",
      from: "Mary Jane",
      description: "Here is your description"
    },
    {
      id: 4,
      title: "Notification 4",
      from: "Mary Jane",
      description: "Here is your description"
    },
    {
      id: 5,
      title: "Notification 5",
      from: "Mary Jane",
      description: "Here is your description"
    },
    {
      id: 6,
      title: "Notification 6",
      from: "Mary Jane",
      description: "Here is your description"
    }
  ]
};
