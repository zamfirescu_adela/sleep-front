export function getNotifications(state) {
  return state.notifications;
}

export function getSelectedNotification(state) {
  return state.selectedNotification;
}

export function getChangeRequests(state) {
  return state.changeRequests;
}

export function getOperationType(state) {
  return state.operationType;
}

export function getChangeLogType(state) {
  return state.changeLogType;
}

export function getChangeLogStatus(state) {
  return state.changeLogStatus;
}

export function getAllGroups(state) {
  return state.allGroups;
}

export function getAllSeries(state) {
  return state.allSeries;
}

export function getAllSpecializations(state) {
  return state.allSeries;
}

export function getAllBuildings(state) {
  return state.allBuildings;
}
