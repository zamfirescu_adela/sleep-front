export function SET_NOTIFICATIONS(state, payload) {
  state.notifications = payload;
}

export function SET_SELECTED_NOTIFICATION(state, payload) {
  state.selectedNotification = payload;
}

export function SET_CHANGE_REQUESTS(state, payload) {
  state.changeRequests = payload;
}

export function SET_ALL_GROUPS(state, payload) {
  state.allGroups = payload;
}

export function SET_ALL_SERIES(state, payload) {
  state.allSeries = payload;
}

export function SET_ALL_SPECIALIZATIONS(state, payload) {
  state.allSpecializations = payload;
}

export function SET_ALL_BUILDINGS(state, payload) {
  state.allBuildings = payload;
}
