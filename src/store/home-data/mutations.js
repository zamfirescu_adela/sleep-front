export function SET_CURRENT_DAY(state) {
  // Sunday 0, Monday 1 ... Saturday 6
  let date = new Date();
  let day = date.getDay();

  if (day === 1) state.currentDay = "luni";
  else if (day === 2) state.currentDay = "marti";
  else if (day === 3) state.currentDay = "miercuri";
  else if (day === 4) state.currentDay = "joi";
  else if (day === 5) state.currentDay = "vineri";
}
