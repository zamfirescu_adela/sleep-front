export function SET_QUOTE(state, payload) {
  state.quote = payload.text;
  state.author = payload.from;
}
