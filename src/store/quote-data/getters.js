export function getQuote(state) {
  return state.quote;
}

export function getAuthor(state) {
  return state.author;
}
