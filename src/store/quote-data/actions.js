import Axios from "axios";

export async function getQuote({ commit }) {
  const response = await Axios.get("https://api.myjson.com/bins/q0kon");
  const filtered = response.data.filter(item => item.text.length < 50);
  const quote = filtered[Math.floor(Math.random() * filtered.length)];
  commit("SET_QUOTE", quote);
}
