import SimpleCrypto from "simple-crypto-js";

export function setEditScheduleStateMode({ commit, state }) {
  commit("SET_STATE_MODE", state.stateModeType.editSchedule);
}
export function setViewScheduleStateMode({ commit, state }) {
  commit("SET_STATE_MODE", state.stateModeType.viewSchedule);
}
export function setAddNotificationStateMode({ commit, state }) {
  commit("SET_STATE_MODE", state.stateModeType.addNotification);
}

export function login({ commit }, payload) {
  const simpleCrypto = new SimpleCrypto(
    "DJ%YU>qRRQeDWpAwr(!iA+lLa5OH_i^-yq>=M<i]Y`sTvJk*u]-Ya}pu%8gn,/9"
  );
  commit("SET_USER", payload);

  localStorage.setItem("data", simpleCrypto.encrypt(payload));
}

export function logout({ commit }) {
  commit("SET_USER", null);
  localStorage.removeItem("data");
}
