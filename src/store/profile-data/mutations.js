export function SET_STATE_MODE(state, payload) {
  state.stateMode = payload;
}

export function SET_IS_LOGGED_ID(state, payload) {
  state.isLoggedIn = payload;
}

export function SET_USER(state, payload) {
  state.user = payload;
}
