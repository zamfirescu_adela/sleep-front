export function getStateMode(state) {
  return state.stateMode;
}

export function getStateModeType(state) {
  return state.stateModeType;
}

export function getManageRolesState(state) {
  return state.isManageRoles;
}

export function getUser(state){
  return state.user;
}