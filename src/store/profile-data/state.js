import SimpleCrypto from "simple-crypto-js";
const simpleCrypto = new SimpleCrypto(
  "DJ%YU>qRRQeDWpAwr(!iA+lLa5OH_i^-yq>=M<i]Y`sTvJk*u]-Ya}pu%8gn,/9"
);

const userData = localStorage.getItem("data");
export default {
  // to be named scheduleStateModeType
  stateModeType: {
    // addSchedule: "ADD_SCHEDULE",
    editSchedule: "EDIT_SCHEDULE",
    // deleteSchedule: "DELETE_SCHEDULE",
    viewSchedule: "VIEW_SCHEDULE",

    addNotification: "ADD_NOTIFICATION"
  },
  stateMode: "VIEW_SCHEDULE",
  user: userData ? JSON.parse(simpleCrypto.decrypt(userData)) : null
};
