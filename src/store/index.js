import Vue from "vue";
import Vuex from "vuex";

import profileData from "./profile-data";
import quoteData from "./quote-data";
import homeData from "./home-data";
import scheduleData from "./schedule-data";
import managementData from "./management-data";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      profileData,
      quoteData,
      homeData,
      scheduleData,
      managementData
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
