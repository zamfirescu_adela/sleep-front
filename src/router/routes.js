const routes = [
  {
    path: "/welcome",
    component: () => import("layouts/LandingPageLayout.vue"),
    children: [
      {
        path: "/",
        component: () => import("pages/LandingPage.vue"),
        name: "LandingPage"
      }
    ]
  },
  {
    path: "/login",
    component: () => import("layouts/LandingPageLayout.vue"),
    children: [
      { path: "/", component: () => import("pages/Auth.vue"), name: "Auth" }
    ]
  },
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    meta: {
      requiresAuth: true
    },
    children: [
      { path: "/", name: "Home", component: () => import("pages/Home.vue") },
      {
        path: "/schedule",
        name: "Schedule",
        component: () => import("pages/Schedule.vue")
      },

      {
        path: "/management",
        name: "Management",
        component: () => import("pages/Management.vue")
      },
      {
        path: "/professors",
        name: "Professors",
        component: () => import("pages/Professors.vue")
      },
      {
        path: "/profile",
        name: "Profile",
        component: () => import("pages/Profile.vue")
      },
      // { path: "/logout", component: () => import("pages/Logout.vue") },
      {
        path: "/roles",
        name: "Roles",
        component: () => import("pages/ManageRoles.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
